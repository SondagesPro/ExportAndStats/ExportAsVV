<?php

/**
 * ExportAsVV Plugin for LimeSurvey
 * Export as a VV file with options
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2021-2025 SondagesPro <https://sondages.pro>
 * @copyright 2021 OECD <https://www.oecd.org>
 * @license AGPL v3
 * @version 0.2.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Affero Public License for more details.
 *
 */
class ExportAsVV extends PluginBase
{

    protected static $name = 'ExportAsVV';
    protected static $description = 'Allow to export response in a VV compatible format.';

    protected $storage = 'DbStorage';
    protected $settings = array(
        'default' => array(
            'type' => 'boolean',
            'label' => 'Use as default export',
            'default' => 0,
        ),
        'sqseparator' => array(
            'type' => 'string',
            'label' => 'Separator between question and Sub question and each subquestions',
            'default' => '\n',
            'htmlOptions' => array(
                 'placeholder' => '\n',
                 'title'=> "default to new line"
            ),
        ),
        'addanswers' => array(
            'type' => 'boolean',
            'label' => 'Add answsers',
            'default' => 1,
        ),
        'questionanswersseparator' => array(
            'type' => 'string',
            'label' => 'Separator between question block and answers block',
            'default' => '\n-\n',
            'htmlOptions' => array(
                 'placeholder' => '\n-\n',
                 'title'=> "default to 2 new lines separated by -"
            ),
        ),
        'answerscodeseparator' => array(
            'type' => 'string',
            'label' => 'Separator between answer code and text',
            'default' => ":",
            'htmlOptions' => array(
                 'placeholder' => ':',
                 'title'=> "default to :"
            ),
        ),
        'answersseparator' => array(
            'type' => 'string',
            'label' => 'Separator between each answers',
            'default' => '\n',
            'htmlOptions' => array(
                 'placeholder' => '\n',
                 'title'=> "default to new line"
            ),
        ),
        'replacelinefeedanswer' => array(
            'type' => 'boolean',
            'label' => 'Replace line feed in free text answers of responses',
            'default' => 0,
        ),
        'vv2compat' => array(
            'type' => 'boolean',
            'label' => 'Replace all reserved names in answer text of responses and in question text (total VV2 compatible)',
            'default' => 0,
        ),
    );

    /* @inheritdoc */
    public function init()
    {
        $this->subscribe('listExportPlugins');
        $this->subscribe('listExportOptions');
        $this->subscribe('newExport');
    }

    /* @see event */
    public function listExportOptions()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->getEvent();
        $type = $event->get('type');

        switch ($type) {
            case 'vv-like':
                $event->set('label', "Export as VV");
                if ($this->get('default', null, null, $this->settings['default']['default'])) {
                    $event->set('default', true);
                }
                break;
        }
    }

    /* @see event */
    public function listExportPlugins()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->getEvent();
        $exports = $event->get('exportplugins');
        $exports['vv-like'] = __CLASS__;
        $event->set('exportplugins', $exports);
    }

    /* @see event */
    public function newExport()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->getEvent();
        $pluginsettings = $this->getPluginSettings(true);
        $writer = new ExportAsVVWriter($pluginsettings);
        $event->set('writer', $writer);
    }
}
