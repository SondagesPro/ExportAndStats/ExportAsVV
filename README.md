# ExportAsVV

Export as VV file with only selected columns and options to get sub questions and answers.

**Warning** : need https://github.com/LimeSurvey/LimeSurvey/pull/2177

## Installation

### Via GIT
- Go to your LimeSurvey Directory (version up to 3.00)
- Clone in plugins/ExportAsVV directory

## Usage

This plugin add an export option _Export as VV_ , the file is a Tab Separated value with 1st line question detail, and 2nd line the question code. Data is compatible with VV import system under condition.

If you have a LimeSurvey without the fix [https://github.com/LimeSurvey/LimeSurvey/pull/2177](Fixed issue #17677: Valid TSV file can not be used in VV import), you must update plugin settings.

### Global plugin settings

- _Use as default export_ if you want this export selected by default when export.
- _Separator between question and Sub question and each subquestions_ By default or if empty use a new line (`\n`) .
- _Add answsers_ Add the answers list after the nquestion in the cells of the 1st line.
- _Separator between question block and answers block_ By default or if empty use 2 new line and a dash (`\n_\n`). Added only if there are answers.
- _Separator between answer code and text_ The separator between code and text of answers. 
- _Separator between each answers_ By default or if empty use a new line (`\n`)
- _Replace line feed in free text answers of responses_ replace line feed and carriage return in free text answers of responses.
- _Replace all reserved names in answer text of responses and in question text (total VV2 compatible)_ This replace all VV reserved caractger like default VV2 export do.
 
Question text and answer text are always cleaned, no tag or line feed inside question text or answers text.

## Contribute and issue

Contribution are welcome, for patch and issue : use [gitlab](https://gitlab.com/SondagesPro/ExportAndStats/ExportAsVV).

## Home page & Copyright
- HomePage <https://www.sondages.pro/>
- Professional support <https://support.sondages.pro/>
- Copyright © 2021-2025 SondagesPro <https://www.sondages.pro>
- Copyright © 2021 OECD <https://www.oecd.org>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro) 
- [Professional support](https://support.sondages.pro/)
