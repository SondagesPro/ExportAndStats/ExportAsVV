<?php

/**
 * Writer for the plugin
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2021-2025 SondagesPro <https://sondages.pro>
 * @copyright 2021 OECD <https://www.oecd.org>
 * @license AGPL v3
 * @since 0.2.1 6 compatibility
 * @since 0.2.0 4 compatibility
 * @since 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Affero Public License for more details.
 *
 */

class ExportAsVVWriter extends CsvWriter
{
    /** @var null|SurveyObj */
    protected $survey;
    /** @var string language */
    public $language;
    /** @var string languageCode */
    public $languageCode;
    /** @var string separator */
    private $separator = '\t';
    /** @var boolean separator */
    private $hasOutputHeader = false;
    /** @var string Separator between question and Sub question */
    private $sqseparator = "\n";
    /** @var string Separator between question and Sub question */
    private $sqsubseparator = "\n";
    /** @var boolean Separator between question and Sub question */
    private $addanswers = true;
    /** @var string Separator between question block and answers block */
    private $questionanswersseparator = "\n-\n";
    /** @var string Separator between question block and answers block */
    private $answerscodeseparator = ":";
    /** @var string Separator between question block and answers block */
    private $answersseparator = "\n";
    /** @var boolean replace line feed in anwser */
    private $replacelinefeedanswer = false;
    /** @var boolean make it totally compatible */
    private $vv2compat = false;

    /**
     * The open filehandle
     */
    private $out = null;

    /** @inheritdoc **/
    public function __construct($pluginsettings)
    {
        if (!empty($pluginsetting['sqseparator']['current'])) {
            $this->qseparator = self::fixStringSetting($pluginsettings['sqseparator']['current']);
        }
        if (isset($pluginsettings['addanswers']['current'])) {
            $this->addanswers = boolval($pluginsettings['addanswers']['current']);
        }
        if (!empty($pluginsettings['questionanswersseparator']['current'])) {
            $this->questionanswersseparator = self::fixStringSetting($pluginsettings['questionanswersseparator']['current']);
        }
        if (!empty($pluginsettings['answerscodeseparator']['current'])) {
            $this->answerscodeseparator = self::fixStringSetting($pluginsettings['answerscodeseparator']['current']);
        }
        if (!empty($pluginsettings['answersseparator']['current'])) {
            $this->answersseparator = self::fixStringSetting($pluginsettings['answersseparator']['current']);
        }
        if (isset($pluginsettings['replacelinefeedanswer']['current'])) {
            $this->replacelinefeedanswer = boolval($pluginsettings['replacelinefeedanswer']['current']);
        }
        if (isset($pluginsettings['replacelinefeedanswer']['current'])) {
            $this->vv2compat = boolval($pluginsettings['vv2compat']['current']);
        }
    }

    /** @inheritdoc **/
    public function init(SurveyObj $survey, $sLanguageCode, FormattingOptions $oOptions)
    {
        $oOptions->headingFormat = "full";
        $oOptions->answerFormat = "short";
        $oOptions->output = "display";
        parent::init($survey, $sLanguageCode, $oOptions);
        $this->csvFilename = "vvexport-survey" . $survey->id . ".csv";
        if ($oOptions->output == 'file') {
            $this->out = fopen($this->filename, 'w');
        } else {
            $this->out = fopen('php://output', 'w');
        }
        $this->survey = $survey;
        $this->language = $sLanguageCode;
    }

    /** @inheritdoc **/
    protected function outputRecord($headers, $values, FormattingOptions $oOptions, $fieldNames = [])
    {
        if (!$this->hasOutputHeader) {
            if ($oOptions->output == 'display') {
                header("Content-Disposition: attachment; filename=" . $this->csvFilename);
                header("Content-type: text/tab-separated-values; charset=UTF-8");
            }
            /* 1st line : full text + info */
            $firstHeader = array();
            foreach ($headers as $header) {
                $firstHeader[] = $this->getdetailedQuestion($this->survey, $oOptions, $header);
            }
            fputcsv($this->out, $firstHeader, "\t");
            /* 2nd line : code */
            $secondHeader = array();
            foreach ($headers as $header) {
                if (isset($this->survey->fieldMap[$header])) {
                    $secondHeader[] = viewHelper::getFieldCode($this->survey->fieldMap[$header], array('LEMcompat' => true));
                } elseif (isset($this->survey->tokenFields[$fieldName])) {
                    $secondHeader[] = "token:" . $fieldName;
                } else {
                    $secondHeader[] = $fieldName;
                }
            }
            fputcsv($this->out, $secondHeader, "\t");
            $this->hasOutputHeader = true;
        }
        fputcsv($this->out, $values, "\t");
    }

    /**
    * Returns the adapted heading using parent function
    *
    * @param Survey $survey
    * @param FormattingOptions $oOptions
    * @param string $fieldName
    * @return string (or false)
    */
    public function getFullHeading(SurveyObj $survey, FormattingOptions $oOptions, $fieldName)
    {
        return $fieldName;
    }

    /**
    * Returns the adapted heading using parent function
    *
    * @param Survey $survey
    * @param FormattingOptions $oOptions
    * @param string $fieldName
    * @return string (or false)
    */
    public function getdetailedQuestion(SurveyObj $survey, FormattingOptions $oOptions, $fieldName)
    {
        $language = $this->language;
        $abbreviated = intval($oOptions->headingTextLength);
        if (empty($abbreviated)) {
            $abbreviated = 15;
        }
        if ($oOptions->headingFormat == 'full') {
            $abbreviated = false;
        }
        $textHead = $fieldName;
        if (isset($survey->fieldMap[$fieldName])) {
            $textHead = viewHelper::getFieldText(
                $survey->fieldMap[$fieldName],
                array(
                    'afterquestion' => '',
                    'separator' => $this->sqseparator, // array($this->sqseparator,'');
                    'abbreviated' => $abbreviated
                )
            );
            if ($this->addanswers && isset($survey->fieldMap[$fieldName]['qid'])) {
                $qid = $survey->fieldMap[$fieldName]['qid'];
                if (intval(App()->getConfig('versionnumber')) <= 3) {
                    $oQuestion = Question::model()->findbyPk(array(
                        'qid' => $qid,
                        'language' => $language
                    ));
                } else {
                    $oQuestion = Question::model()->findbyPk($qid);
                }
                if (!empty($survey->answers[$qid])) {
                    $answersScales = $survey->answers[$qid];
                    $aAnswsersText = array();
                    if (count($answersScales) > 1) {
                        $aAnswsersText[] = gT("Scale 1", 'unescaped', $language);
                    }
                    $scale = 1;
                    while (count($answersScales) > 0) {
                        if ($scale > 1) {
                            $aAnswsersText[] = gT("Scale $scale", 'unescaped', $language) ;
                        }
                        $answers = array_shift($answersScales);
                        foreach ($answers as $code => $answer) {
                            $aAnswsersText[] = $code . $this->answerscodeseparator . viewHelper::flatten($answer);
                        }
                        $scale++;
                    }
                }
                if ($oQuestion && in_array($oQuestion->type, array("L","!","P","M")) && $oQuestion->other == "Y") {
                    $aAnswsersText[] = "-oth-" . $this->answerscodeseparator . gT("Other", 'unescaped', $language);
                }
                if (!empty($aAnswsersText)) {
                    $textHead .= "{$this->questionanswersseparator}";
                    $textHead .= implode("{$this->answersseparator}", $aAnswsersText);
                }
            }
        } elseif (isset($survey->tokenFields[$fieldName])) {
            $textHead = $survey->tokenFields[$fieldName]['description'];
        }
        return $textHead;
    }

    /**
    * Performs a transformation of the response value.
    * Reload the survey to use own value
    *
    * @param string $sValue
    * @param string $fieldType
    * @param FormattingOptions $oOptions
    * @param string $sColumn The name of the column
    * @return string
    */
    protected function transformResponseValue($sValue, $fieldType, FormattingOptions $oOptions, $sColumn = null)
    {
        if (is_null($sValue)) {
            return "{question_not_shown}";
        }
        if ($this->vv2compat) {
            $sValue = str_replace(
                array("{", "\n", "\r", "\t"),
                array("{lbrace}", "{newline}", "{cr}", "{tab}"),
                $sValue
            );
            $sValue = preg_replace('/^"/', '{quote}', $sValue);
        } elseif ($this->replacelinefeedanswer) {
            $sValue = str_replace(
                array("\n", "\r"),
                array("{newline}", "{cr}"),
                $sValue
            );
        }
        return $sValue;
    }

    /** @inheritdoc */
    public function close()
    {
        if (!is_null($this->out)) {
            fclose($this->out);
        }
    }

    /**
     * Replace \n and \t by line feed and tab
     * @param string
     * @return string
     */
    private static function fixStringSetting($string)
    {
        return str_replace(
            array('\n', '\t'),
            array("\n"," \t"),
            $string
        );
    }
}
